package com.example.database.di

import android.app.Application
import android.content.Context
import com.example.database.AppDatabase
import com.example.database.dao.DeveloperDao
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DbModule(val application: Application) {

    @ApplicationContext
    @Provides
    fun provideApplicationContext(): Context = application.applicationContext

    @Provides
    @Singleton
    internal fun appDatabase(
        @ApplicationContext context: Context
    ): AppDatabase =
        AppDatabase.getInstance(
            context
        )

    @Provides
    @Singleton
    internal fun beerDao(db: AppDatabase): DeveloperDao = db.beerDao()
}
