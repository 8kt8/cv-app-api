package com.example.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.database.AppDatabase.Companion.DATABASE_VERSION
import com.example.database.dao.DeveloperDB
import com.example.database.dao.DeveloperDao

@Database(
    entities = [
       DeveloperDB::class
    ],
    version = DATABASE_VERSION
)
abstract class AppDatabase: RoomDatabase() {

    companion object {
        const val DATABASE_VERSION = 1
        private const val DATABASE_NAME = "developers.db"

        @Volatile
        private var instance: AppDatabase? = null

        fun getInstance(
            context: Context
        ): AppDatabase =
            instance ?: synchronized(this) {
                instance
                    ?: buildDatabase(context).also {
                        instance = it
                    }
            }

        private fun buildDatabase(
            context: Context
        ): AppDatabase {
            return Room.databaseBuilder(
                context,
                AppDatabase::class.java,
                DATABASE_NAME
            ).build()
        }

    }

    abstract fun beerDao(): DeveloperDao
}