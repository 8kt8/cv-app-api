package com.example.database.dao

import androidx.room.*
import io.reactivex.Completable
import io.reactivex.Flowable

@Dao
abstract class DeveloperDao {

    @get:Transaction
    @get:Query("SELECT * FROM developer")
    abstract val all: Flowable<List<DeveloperDB>>

    @Transaction
    @Query("SELECT * FROM developer WHERE developer.id = :id")
    abstract fun getById(id: Int): Flowable<DeveloperDB>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertAll(developer: List<DeveloperDB>): Completable
}