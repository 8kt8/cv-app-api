package com.example.database.dao

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(
    tableName = "Developer"
)
data class DeveloperDB(
    @field:PrimaryKey val id: Int,
    val firstName: String,
    val lastName: String,
    val photo_url: String,
    val dateOfBirth: String,
    val about: String,
    val contactDB: String,
    val educationDB: String,
    val experienceDB: String,
    val skillsDB: String,
    val programingLanguagesDB: String,
    val trainingsDB: String,
    val interestsDB: String
)