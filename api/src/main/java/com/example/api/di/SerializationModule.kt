package com.example.api.di

import com.example.api.serialization.JsonAdapter
import com.example.api.serialization.MoshiAdapter
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import dagger.Binds
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
abstract class SerializationModule {

    @Binds
    internal abstract fun bindJsonAdapter(
        moshiAdapter: MoshiAdapter
    ): JsonAdapter

    @Module
    companion object {

        @JvmStatic
        @Provides
        @Singleton
        internal fun provideMoshi(): Moshi {
            return Moshi.Builder()
                .add(KotlinJsonAdapterFactory())
                .build()
        }
    }
}
