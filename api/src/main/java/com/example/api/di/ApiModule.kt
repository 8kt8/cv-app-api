package com.example.api.di

import android.app.Application
import android.content.Context
import com.example.api.ApiConfig
import com.example.api.CvApi
import com.example.api.repository.RepositoryModule
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit

import javax.inject.Singleton

@Module(
    includes = [
        HttpModule::class,
        RetrofitModule::class,
        SerializationModule::class,
        RxJavaModule::class,
        RepositoryModule::class
    ]
)
class ApiModule(val application: Application) {

    @ApplicationContext
    @Provides
    fun provideApplicationContext(): Context = application.applicationContext

    @Provides
    @Singleton
    internal fun punkApi(
        retrofitBuilder: Retrofit.Builder,
        config: ApiConfig
    ): CvApi =
        retrofitBuilder.createService(
            config.cvApiUrl(),
            CvApi::class.java
        )
}