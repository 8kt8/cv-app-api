package com.example.api

class ApiConfig {

    val apiUrl = "https://my-json-server.typicode.com/"
    private val userName = "8kt8"
    private val projectName = "CvApi"
    val readTimeout = 60L
    val connectionTimeout = 60L

    fun cvApiUrl() = "$apiUrl$userName/$projectName/"
}