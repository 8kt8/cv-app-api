package com.example.api.rx

import io.reactivex.Scheduler

interface CoreSchedulers {
	val computation: Scheduler
	val dbIO: Scheduler
	val diskIO: Scheduler
	val networkIO: Scheduler
	val mainThread: Scheduler
}