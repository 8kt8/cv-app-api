package com.example.api

import com.example.api.model.response.DeveloperResponse
import io.reactivex.Single
import retrofit2.http.GET

interface CvApi {

    @GET("developers")
    fun getAll(): Single<List<DeveloperResponse>>
}