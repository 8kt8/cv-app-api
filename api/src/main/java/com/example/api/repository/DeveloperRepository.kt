package com.example.api.repository

import com.example.api.model.domain.Developer
import io.reactivex.Completable
import io.reactivex.Flowable

interface DeveloperRepository {
    fun refresh(): Completable
    fun getById(id: Int): Flowable<Developer>
    fun getAll(): Flowable<List<Developer>>
}