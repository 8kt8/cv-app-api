package com.example.api.repository.dbMapper

import com.example.api.common.fastLazy
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types
import javax.inject.Inject

class ListDBMapper @Inject constructor(moshi: Moshi) {

    private var type = Types.newParameterizedType(
        List::class.java,
        String::class.java
    )

    private val methodAdapter
            by fastLazy<JsonAdapter<List<String>>> { moshi.adapter(type) }

    fun map(list: List<String>?) = methodAdapter.toJson(list)

    fun mapDB(string: String) = methodAdapter.fromJson(string)
}