package com.example.api.repository

import dagger.Binds
import dagger.Module

@Module
abstract class RepositoryModule {

    @Binds
    internal abstract fun bindDeveloperRepository(developerRepository: DeveloperRepositoryImpl): DeveloperRepository
}