package com.example.api.repository.interactor

import com.example.api.repository.DeveloperRepository
import javax.inject.Inject

class GetDeveloperByIdInteractor @Inject constructor(
   private val developerRepository: DeveloperRepository
) {
    fun getById(id: Int) = developerRepository.getById(id)
}