package com.example.api.repository.dbMapper

import com.example.api.common.fastLazy
import com.example.api.model.domain.Contact
import com.squareup.moshi.Moshi
import javax.inject.Inject

class ContactDBMapper @Inject constructor(moshi: Moshi) {

    private val methodAdapter by fastLazy { moshi.adapter(Contact::class.java) }

    fun map(contact: Contact?) = methodAdapter.toJson(contact)

    fun mapDB(contactDB: String) = methodAdapter.fromJson(contactDB)
}