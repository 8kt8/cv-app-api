package com.example.api.repository.dbMapper

import com.example.api.common.fastLazy
import com.example.api.model.domain.education.Education
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types
import javax.inject.Inject

class EducationDBMapper @Inject constructor(moshi: Moshi) {

    private var type = Types.newParameterizedType(
        List::class.java,
        Education::class.java
    )

    private val methodAdapter
            by fastLazy<JsonAdapter<List<Education>>> { moshi.adapter(type) }

    fun map(education: List<Education>?) = methodAdapter.toJson(education)

    fun mapDB(educationDB: String) = methodAdapter.fromJson(educationDB)
}