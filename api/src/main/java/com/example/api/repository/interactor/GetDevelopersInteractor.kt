package com.example.api.repository.interactor

import com.example.api.repository.DeveloperRepository
import javax.inject.Inject

class GetDevelopersInteractor @Inject constructor(
    private val developerRepository: DeveloperRepository
) {
    fun getAll() = developerRepository.getAll()
}