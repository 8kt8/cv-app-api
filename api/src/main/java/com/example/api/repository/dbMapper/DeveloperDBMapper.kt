package com.example.api.repository.dbMapper

import com.example.api.model.domain.Contact
import com.example.api.model.domain.Developer
import com.example.database.dao.DeveloperDB
import javax.inject.Inject

class DeveloperDBMapper @Inject constructor(
    private val contactDBMapper: ContactDBMapper,
    private val educationDBMapper: EducationDBMapper,
    private val experienceDBMapper: ExperienceDBMapper,
    private val listDBMapper: ListDBMapper
) {

    fun map(developers: List<Developer>): List<DeveloperDB> = developers.map(::map)

    fun map(developer: Developer): DeveloperDB = with(developer){
        DeveloperDB(
            id = id,
            firstName = firstName,
            lastName = lastName,
            photo_url = photo_url,
            dateOfBirth = dateOfBirth,
            about = about,
            contactDB = contactDBMapper.map(contact),
            educationDB = educationDBMapper.map(education),
            experienceDB = experienceDBMapper.map(experience),
            skillsDB = listDBMapper.map(skills),
            programingLanguagesDB = listDBMapper.map(programingLanguages),
            trainingsDB = listDBMapper.map(trainings),
            interestsDB = listDBMapper.map(interests)
        )
    }

    fun mapDB(developers: List<DeveloperDB>): List<Developer> = developers.map(::mapDB)

    fun mapDB(developerDB: DeveloperDB): Developer = with(developerDB) {
        Developer(
            id = id,
            firstName = firstName,
            lastName = lastName,
            photo_url = photo_url,
            dateOfBirth = dateOfBirth,
            about = about,
            contact = contactDBMapper.mapDB(contactDB) ?: Contact.EMPTY,
            education = educationDBMapper.mapDB(educationDB) ?: emptyList(),
            experience = experienceDBMapper.mapDB(experienceDB) ?: emptyList(),
            skills = listDBMapper.mapDB(skillsDB),
            programingLanguages = listDBMapper.mapDB(programingLanguagesDB),
            trainings = listDBMapper.mapDB(trainingsDB),
            interests = listDBMapper.mapDB(interestsDB)
        )
    }
}