package com.example.api.repository.dbMapper

import com.example.api.common.fastLazy
import com.example.api.model.domain.experience.Experience
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types
import javax.inject.Inject

class ExperienceDBMapper @Inject constructor(moshi: Moshi) {

    private var type = Types.newParameterizedType(
        List::class.java,
        Experience::class.java
    )

    private val methodAdapter
            by fastLazy<JsonAdapter<List<Experience>>> { moshi.adapter(type) }

    fun map(experience: List<Experience>?) = methodAdapter.toJson(experience)

    fun mapDB(experienceDB: String) = methodAdapter.fromJson(experienceDB)
}