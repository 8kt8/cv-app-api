package com.example.api.repository.interactor

import com.example.api.repository.DeveloperRepository
import javax.inject.Inject

class RefreshDeveloperInteractor @Inject constructor(
    private val developerRepository: DeveloperRepository
) {

    fun refresh() = developerRepository.refresh()
}