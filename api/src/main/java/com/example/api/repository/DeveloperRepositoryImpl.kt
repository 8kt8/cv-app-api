package com.example.api.repository

import com.example.api.CvApi
import com.example.api.model.domain.Developer
import com.example.api.model.domain.DeveloperMapper
import com.example.api.repository.dbMapper.DeveloperDBMapper
import com.example.api.rx.CoreSchedulers
import com.example.database.dao.DeveloperDao
import io.reactivex.Completable
import io.reactivex.Flowable
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class DeveloperRepositoryImpl @Inject constructor(
    private val cvApi: CvApi,
    private val developerDao: DeveloperDao,
    private val developerMapper: DeveloperMapper,
    private val developerDBMapper: DeveloperDBMapper,
    private val coreSchedulers: CoreSchedulers
): DeveloperRepository{

    override fun refresh(): Completable = cvApi.getAll()
            .map(developerMapper::map)
            .subscribeOn(coreSchedulers.networkIO)
            .map(developerDBMapper::map)
            .flatMapCompletable(developerDao::insertAll)
            .subscribeOn(coreSchedulers.dbIO)

    override fun getById(id: Int): Flowable<Developer> = developerDao.getById(id)
        .map(developerDBMapper::mapDB)
        .subscribeOn(coreSchedulers.dbIO)

    override fun getAll(): Flowable<List<Developer>> = developerDao.all
        .map(developerDBMapper::mapDB)
        .subscribeOn(coreSchedulers.dbIO)

}