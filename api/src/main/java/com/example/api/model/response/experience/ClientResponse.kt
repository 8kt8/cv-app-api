package com.example.api.model.response.experience

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ClientResponse(
    @Json(name = "name") val name: String,
    @Json(name = "url") val url: String,
    @Json(name = "logo_url") val logoUrl: String
)