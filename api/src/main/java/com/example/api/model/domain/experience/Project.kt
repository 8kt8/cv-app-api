package com.example.api.model.domain.experience

data class Project(
    val name: String,
    val description: String,
    val startDate: String,
    val endDate: String,
    val client: Client
)