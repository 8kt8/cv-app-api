package com.example.api.model.domain.experience

data class Experience(
    val companyName: String,
    val url: String,
    val logoUrl: String,
    val positions: List<Position>
)