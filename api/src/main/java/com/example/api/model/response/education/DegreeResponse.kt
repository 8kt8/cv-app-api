package com.example.api.model.response.education

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class DegreeResponse (
	@Json(name = "name") val name : String,
	@Json(name = "field_of_study") val fieldOfStudy : String,
	@Json(name = "start_year") val start_year : String,
	@Json(name = "end_year") val end_year : String
)