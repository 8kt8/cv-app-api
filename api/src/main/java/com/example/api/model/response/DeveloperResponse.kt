package com.example.api.model.response

import com.example.api.model.response.education.EducationResponse
import com.example.api.model.response.experience.ExperienceResponse
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class DeveloperResponse(
    @Json(name = "id") val id: Int,
    @Json(name = "first_name") val firstName: String,
    @Json(name = "last_name") val lastName: String,
    @Json(name = "photo_url") val photo_url: String,
    @Json(name = "date_of_birth") val dateOfBirth: String,
    @Json(name = "about") val about: String,
    @Json(name = "contact") val contact: ContactResponse,
    @Json(name = "education") val education: List<EducationResponse>,
    @Json(name = "experience") val experience: List<ExperienceResponse>,
    @Json(name = "skills") val skills: List<String>,
    @Json(name = "programing_languages") val programingLanguages: List<String>,
    @Json(name = "trainings") val trainings: List<String>,
    @Json(name = "interests") val interests: List<String>
)