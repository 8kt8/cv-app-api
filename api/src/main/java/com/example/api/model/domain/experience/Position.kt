package com.example.api.model.domain.experience

data class Position (
	val name: String,
	val city: String,
	val country: String,
	val district: String,
	val startDate: String,
	val endDate: String,
	val projects: List<Project>
)