package com.example.api.model.domain

import com.example.api.model.domain.education.Education
import com.example.api.model.domain.experience.Experience

data class Developer(
    val id: Int,
    val firstName: String,
    val lastName: String,
    val photo_url: String,
    val dateOfBirth: String,
    val about: String,
    val contact: Contact?,
    val education: List<Education>?,
    val experience: List<Experience>?,
    val skills: List<String>?,
    val programingLanguages: List<String>?,
    val trainings: List<String>?,
    val interests: List<String>?
)