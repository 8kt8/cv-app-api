package com.example.api.model.response

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ContactResponse(
    @Json(name = "email") val email: String,
    @Json(name = "phone_number") val phoneNumber: String,
    @Json(name = "linkedin_url") val linkedinUrl: String
)