package com.example.api.model.domain

import com.example.api.model.domain.education.EducationMapper
import com.example.api.model.domain.experience.ExperienceMapper
import com.example.api.model.response.DeveloperResponse
import javax.inject.Inject

class DeveloperMapper @Inject constructor(
    private val contactMapper: ContactMapper,
    private val educationMapper: EducationMapper,
    private val experienceMapper: ExperienceMapper
) {

    fun map(developers: List<DeveloperResponse>): List<Developer> = developers.map(::map)

    fun map(developerResponse: DeveloperResponse) = with(developerResponse){
        Developer(
            id = id,
            firstName = firstName,
            lastName = lastName,
            photo_url = photo_url,
            dateOfBirth = dateOfBirth,
            about = about,
            contact = contactMapper.map(contact),
            education = educationMapper.map(education),
            experience = experienceMapper.map(experience),
            skills = skills,
            programingLanguages = programingLanguages,
            trainings = trainings,
            interests = interests
        )
    }
}