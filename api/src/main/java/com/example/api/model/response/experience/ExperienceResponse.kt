package com.example.api.model.response.experience

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ExperienceResponse(
    @Json(name = "company_name") val companyName: String,
    @Json(name = "url") val url: String,
    @Json(name = "logo_url") val logoUrl: String,
    @Json(name = "positions") val positions: List<PositionResponse>
)