package com.example.api.model.domain.experience

import com.example.api.model.response.experience.ExperienceResponse
import javax.inject.Inject

class ExperienceMapper @Inject constructor(
    private val positionMapper: PositionMapper
) {

    fun map(experienceResponse: List<ExperienceResponse>): List<Experience> = experienceResponse.map(::map)

    fun map(experienceResponse: ExperienceResponse) = with(experienceResponse){
        Experience(
            companyName = companyName,
            url = url,
            logoUrl = logoUrl,
            positions = positionMapper.map(positions)
        )
    }
}