package com.example.api.model.domain.education

import com.example.api.model.response.education.DegreeResponse
import javax.inject.Inject

class DegreeMapper @Inject constructor() {

    fun map(degreesResponse: List<DegreeResponse>): List<Degree> = degreesResponse.map(::map)

    fun map(degreeResponse: DegreeResponse) = with(degreeResponse){
        Degree(
            name = name,
            startYear = start_year,
            endYear = end_year,
            fieldOfStudy = fieldOfStudy
        )
    }
}