package com.example.api.model.domain

data class Contact(
    val email: String,
    val phoneNumber: String,
    val linkedinUrl: String
){

    companion object {
        val EMPTY = (Contact("-", "-", "-"))
    }
}