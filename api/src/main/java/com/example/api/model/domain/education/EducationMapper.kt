package com.example.api.model.domain.education

import com.example.api.model.response.education.EducationResponse
import javax.inject.Inject

class EducationMapper @Inject constructor(
    private val degreeMapper: DegreeMapper
) {

    fun map(educationResponse: List<EducationResponse>): List<Education> = educationResponse.map(::map)

    fun map(educationResponse: EducationResponse) = with(educationResponse){
        Education(
            schoolName = schoolName,
            degrees = degreeMapper.map(degrees),
            logoUrl = logoUrl,
            url = url
        )
    }
}