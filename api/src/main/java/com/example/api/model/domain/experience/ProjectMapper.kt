package com.example.api.model.domain.experience

import com.example.api.model.response.experience.ProjectResponse
import javax.inject.Inject

class ProjectMapper @Inject constructor(
    private val clientMapper: ClientMapper
) {

    fun map(projectsResponse: List<ProjectResponse>): List<Project> = projectsResponse.map(::map)

    fun map(projectResponse: ProjectResponse) = with(projectResponse){
        Project(
            name = name,
            startDate = startDate,
            endDate = endDate,
            client = clientMapper.map(client),
            description = description
        )
    }
}