package com.example.api.model.domain

import com.example.api.model.response.ContactResponse
import javax.inject.Inject

class ContactMapper @Inject constructor() {

    fun map(contactResponse: ContactResponse) = with(contactResponse){
        Contact(
            email = email,
            phoneNumber = phoneNumber,
            linkedinUrl = linkedinUrl
        )
    }
}