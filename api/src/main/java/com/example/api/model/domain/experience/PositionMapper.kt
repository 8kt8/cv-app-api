package com.example.api.model.domain.experience

import com.example.api.model.response.experience.PositionResponse
import javax.inject.Inject

class PositionMapper @Inject constructor(
    private val projectMapper: ProjectMapper
) {

    fun map(positions: List<PositionResponse>): List<Position> = positions.map(::map)

    fun map(positionResponse: PositionResponse) = with(positionResponse){
        Position(
            name = name,
            city = city,
            country = country,
            district = district,
            endDate = endDate,
            startDate = startDate,
            projects = projectMapper.map(projects)
            )
    }
}