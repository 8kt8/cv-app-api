package com.example.api.model.domain.education

data class Degree(
	val name : String,
	val fieldOfStudy : String,
	val startYear : String,
	val endYear : String
)