package com.example.api.model.domain.experience

import com.example.api.model.response.experience.ClientResponse
import javax.inject.Inject

class ClientMapper @Inject constructor() {

    fun map(clientResponse: ClientResponse) = with(clientResponse){
        Client(
            name = name,
            url = url,
            logoUrl = logoUrl
        )
    }
}