package com.example.api.model.domain.experience

data class Client(
    val name: String,
    val url: String,
    val logoUrl: String
)