package com.example.api.model.domain.education


class Education(
    val schoolName: String,
    val url: String,
    val logoUrl: String,
    val degrees: List<Degree>
)