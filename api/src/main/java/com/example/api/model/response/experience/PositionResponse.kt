package com.example.api.model.response.experience

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class PositionResponse (
	@Json(name = "name") val name: String,
	@Json(name = "city") val city: String,
	@Json(name = "country") val country: String,
	@Json(name = "district") val district: String,
	@Json(name = "start_date") val startDate: String,
	@Json(name = "end_date") val endDate: String,
	@Json(name = "projects") val projects: List<ProjectResponse>
)