package com.example.api.model.response.education

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class EducationResponse(
    @Json(name = "school_name") val schoolName: String,
    @Json(name = "url") val url: String,
    @Json(name = "logo_url") val logoUrl: String,
    @Json(name = "degrees") val degrees: List<DegreeResponse>
)