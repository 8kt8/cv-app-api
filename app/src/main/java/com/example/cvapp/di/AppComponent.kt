package com.example.cvapp.di

import com.example.api.di.ApiModule
import com.example.cvapp.common.AppApplication
import com.example.database.di.DbModule
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AppModule::class,
        ApiModule::class,
        LogModule::class,
        DbModule::class
    ]
)
interface AppComponent {
    fun presentationComponent(presentationModule: PresentationModule): PresentationComponent

    fun inject(application: AppApplication)

}

