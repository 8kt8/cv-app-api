package com.example.cvapp.di

import com.orhanobut.logger.AndroidLogAdapter
import com.orhanobut.logger.FormatStrategy
import com.orhanobut.logger.PrettyFormatStrategy
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class LogModule {

    @Provides
    @Singleton
    fun provideFormatStrategy(): FormatStrategy {
        return PrettyFormatStrategy.newBuilder()
            .showThreadInfo(true)  // (Optional) Whether to show thread info or not. Default true
            .methodCount(4)         // (Optional) How many method line to show. Default 4
            .methodOffset(0)        // (Optional) Skips some method invokes in stack trace. Default 5
            .build()
    }

    @Singleton
    @Provides
    internal fun provideLogAdapter(formatStrategy: FormatStrategy): AndroidLogAdapter =
        AndroidLogAdapter(formatStrategy)

}

