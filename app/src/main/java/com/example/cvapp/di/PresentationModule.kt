package com.example.cvapp.di

import android.app.Activity
import android.content.Context
import android.content.res.Resources
import androidx.fragment.app.FragmentActivity
import dagger.Module
import dagger.Provides

@Module
class PresentationModule(
    private val activity: FragmentActivity
) {

    @Provides
    fun provideContext(): Context = activity

    @Provides
    fun provideActivity(): Activity = activity

    @Provides
    fun provideFragmentActivity(): FragmentActivity = activity

    @Provides
    internal fun provideResources(context: Context): Resources {
        return context.resources
    }
}
