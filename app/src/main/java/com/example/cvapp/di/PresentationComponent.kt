package com.example.cvapp.di

import com.example.cvapp.ui.developerContact.di.DeveloperContactModule
import com.example.cvapp.ui.developerDetails.FragmentDeveloperDetails
import com.example.cvapp.ui.developerDetails.di.DeveloperDetailsModule
import com.example.cvapp.ui.educationDetails.FragmentEducationDetails
import com.example.cvapp.ui.educationDetails.di.EducationDetailsModule
import com.example.cvapp.ui.experienceDetails.FragmentExperienceDetails
import com.example.cvapp.ui.experienceDetails.di.ExperienceDetailsModule
import com.example.cvapp.ui.main.BaseFragment
import com.example.cvapp.ui.main.MainActivity
import dagger.Subcomponent

@ActivityScope
@Subcomponent(
    modules = [
        PresentationModule::class,
        ViewModelModule::class,
        DeveloperDetailsModule::class,
        ExperienceDetailsModule::class,
        EducationDetailsModule::class,
        DeveloperContactModule::class
    ]
)
interface PresentationComponent {

    fun inject(activity: MainActivity)

    fun inject(baseFragment: BaseFragment)

    fun inject(fragmentDeveloperDetails: FragmentDeveloperDetails)

    fun inject(fragmentExperienceDetails: FragmentExperienceDetails)

    fun inject(fragmentEducationDetails: FragmentEducationDetails)
}
