package com.example.cvapp.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.cvapp.ui.main.BaseViewModel
import com.example.cvapp.ui.main.CoreViewModelFactory
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {
	@Binds
	@IntoMap
	@ViewModelKey(BaseViewModel::class)
	internal abstract fun bindBaseViewModel(baseViewModel: BaseViewModel): ViewModel

	@Binds
	internal abstract fun bindViewModelFactory(factory: CoreViewModelFactory): ViewModelProvider.Factory
}