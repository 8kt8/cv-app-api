package com.example.cvapp.common

import android.app.Application
import com.example.api.common.fastLazy
import com.example.api.di.ApiModule

import com.example.cvapp.di.AppComponent

import com.example.cvapp.di.DaggerAppComponent
import com.example.database.di.DbModule
import com.orhanobut.logger.AndroidLogAdapter
import com.orhanobut.logger.Logger
import javax.inject.Inject

open class AppApplication: Application() {

    @Inject
    lateinit var androidLogAdapter: AndroidLogAdapter

    open val appComponent: AppComponent by fastLazy {
        DaggerAppComponent.builder()
            .apiModule(ApiModule(this))
            .dbModule(DbModule(this))
            .build()
    }

    override fun onCreate() {
        appComponent.inject(this)
        super.onCreate()
        addLogging()
    }

    private fun addLogging() = Logger.addLogAdapter(androidLogAdapter)
}