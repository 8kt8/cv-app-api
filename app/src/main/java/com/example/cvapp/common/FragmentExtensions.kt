package com.example.cvapp.common

import androidx.fragment.app.Fragment
import com.example.cvapp.di.DependencyInjectionActivity
import com.example.cvapp.di.PresentationComponent
import com.example.cvapp.di.PresentationModule

fun Fragment.getPresentationComponent(): PresentationComponent {
    return when (val activity = requireActivity()) {
        is DependencyInjectionActivity -> activity.presentationComponent
        else -> {
            // android test use own activity for tests (androidx.fragment.app.testing.FragmentScenario.EmptyFragmentActivity)
            // so it can't be casted to DependencyInjectionActivity, PresentationComponent new instance must be created in this case
            (activity.application as AppApplication).appComponent.presentationComponent(
                PresentationModule(activity)
            )
        }
    }
}
