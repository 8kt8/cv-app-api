package com.example.cvapp.common.converter

import org.joda.time.DateTime
import javax.inject.Inject

class DateTimeConverter @Inject constructor() {

    fun convert(value: String?) = value?.let(::DateTime)

}