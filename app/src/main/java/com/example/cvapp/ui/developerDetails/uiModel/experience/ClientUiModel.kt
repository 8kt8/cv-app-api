package com.example.cvapp.ui.developerDetails.uiModel.experience

data class ClientUiModel(
    val name: String,
    val url: String,
    val logoUrl: String
)