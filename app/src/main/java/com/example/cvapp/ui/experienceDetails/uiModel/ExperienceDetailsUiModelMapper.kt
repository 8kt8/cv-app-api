package com.example.cvapp.ui.experienceDetails.uiModel

import com.example.api.model.domain.experience.Experience
import com.example.cvapp.ui.DisplayedExperienceTimeRangeFactory
import javax.inject.Inject

class ExperienceDetailsUiModelMapper @Inject constructor(
    private val timeRangeFactory: DisplayedExperienceTimeRangeFactory,
    private val experienceUiModelMapper: ExperienceUiModelMapper
) {

    fun map(experience: Experience) =
        ExperienceDetailsUiModel(
            companyName = experience.companyName,
            timeRange = timeRangeFactory.create(experience),
            companyLogoUrl = experience.logoUrl,
            companyUrl = experience.url,
            projectUiModels = experienceUiModelMapper.map(experience)
        )
}

