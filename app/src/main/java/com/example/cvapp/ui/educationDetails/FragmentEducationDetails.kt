package com.example.cvapp.ui.educationDetails

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import androidx.navigation.fragment.navArgs
import com.example.api.common.fastLazy
import com.example.cvapp.R
import com.example.cvapp.common.getPresentationComponent
import com.example.cvapp.databinding.FragmentEducationDetailsBinding
import com.example.cvapp.ui.developerContact.DeveloperContactViewModel
import com.example.cvapp.ui.educationDetails.uiComponent.FragmentEducationDetailsUiComponent
import com.example.cvapp.ui.main.BindingFragment
import javax.inject.Inject

class FragmentEducationDetails: BindingFragment<FragmentEducationDetailsBinding>(),
    FragmentEducationDetailsUiComponentHost {

    @Inject
    lateinit var uiComponent: FragmentEducationDetailsUiComponent

    override fun onAttach(context: Context) {
        super.onAttach(context)
        getPresentationComponent().inject(this)
    }

    override val viewModel
            by fastLazy { forFragment<EducationDetailsViewModel>() }

    override val viewModelContact: DeveloperContactViewModel
            by fastLazy { forActivity<DeveloperContactViewModel>() }

    override val layoutId: Int = R.layout.fragment_education_details

    override val componentAppBarBinding
        get() = binding.componentAppBar

    override val imageVieAvatarIcon: ImageView
        get() = binding.profileImage

    override fun isAvatarShown(): Boolean = viewModel.isAvatarShown

    override fun setIsAvatarShown(isShown: Boolean) {
        viewModel.isAvatarShown = isShown
    }

    override fun startIntent(intent: Intent, chooserTitle: String) {
        requireActivity().startActivity(Intent.createChooser(intent, chooserTitle))
    }

    private val args: FragmentEducationDetailsArgs by navArgs()

    override val developerId: Int
        get() = args.developerId

    override val educationId: Int
        get() = args.educationId

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        uiComponent.init(this)
    }
}