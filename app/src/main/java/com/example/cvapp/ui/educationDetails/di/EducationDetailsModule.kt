package com.example.cvapp.ui.educationDetails.di

import androidx.lifecycle.ViewModel
import com.example.cvapp.di.ViewModelKey
import com.example.cvapp.ui.educationDetails.EducationDetailsViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class EducationDetailsModule {
    @Binds
    @IntoMap
    @ViewModelKey(EducationDetailsViewModel::class)
    internal abstract fun bindExperienceDetailsViewModel(viewModel: EducationDetailsViewModel): ViewModel

}