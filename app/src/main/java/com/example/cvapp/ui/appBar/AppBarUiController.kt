package com.example.cvapp.ui.appBar

import android.content.Intent
import android.net.Uri
import com.example.cvapp.R
import com.example.cvapp.ui.StringProvider
import com.example.cvapp.ui.uiController.UiController
import com.google.android.material.appbar.AppBarLayout
import javax.inject.Inject
import kotlin.math.abs


class AppBarUiController @Inject constructor(
    private val stringProvider: StringProvider
) : UiController<AppBarUiComponent>() {

    private var mMaxScrollSize = 0

    fun onOffsetChanged(appBarLayout: AppBarLayout, i: Int) {
        val isAvatarShown = uiComponent.host.isAvatarShown()
        if (mMaxScrollSize == 0) mMaxScrollSize = appBarLayout.totalScrollRange
        val percentage: Int = abs(i) * 100 / mMaxScrollSize
        if (percentage >= PERCENTAGE_TO_ANIMATE_AVATAR && isAvatarShown) {
            setIsAvatarShown(false)
            uiComponent.showImage()
        }
        if (percentage <= PERCENTAGE_TO_ANIMATE_AVATAR && !isAvatarShown) {
            setIsAvatarShown(true)
            uiComponent.hideImage()
        }
    }

    fun onGmailClick() {
        uiComponent.host.viewModelContact.contact?.email?.let {
            val intent = Intent(Intent.ACTION_SEND).apply {
                data = Uri.parse(it)
                type = "message/rfc822"
                putExtra(Intent.EXTRA_EMAIL, arrayOf(it))
                putExtra(Intent.EXTRA_SUBJECT, stringProvider.provideString(R.string.email_subject))
                putExtra(
                    Intent.EXTRA_TEXT,
                    stringProvider.provideString(R.string.email_message)
                )
            }
            uiComponent.host.startIntent(
                intent,
                stringProvider.provideString(R.string.email_chooser_title)
            )
        }
    }

    fun onUrlHandle(url: String) = startIntent(url)

    fun onUrlClick() {
        uiComponent.host.viewModelContact.contact?.linkedinUrl?.let {
            startIntent(it)
        }
    }

    private fun startIntent(url: String){
        val intent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
        uiComponent.host.startIntent(
            intent,
            stringProvider.provideString(R.string.browser_chooser_title)
        )
    }

    fun onContactClick() {
        uiComponent.host.viewModelContact.contact?.phoneNumber?.let {
            val intent =
                Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", it, null))
            uiComponent.host.startIntent(
                intent,
                stringProvider.provideString(R.string.contact_chooser_title)
            )
        }
    }

    fun onRestoreIconState() = with(uiComponent) {
        if (host.isAvatarShown()) {
            hideImage(0)
        } else {
            showImage(0)
        }
    }

    private fun setIsAvatarShown(isAvatarShown: Boolean) =
        uiComponent.host.setIsAvatarShown(isAvatarShown)

    companion object {
        private const val PERCENTAGE_TO_ANIMATE_AVATAR = 50
    }
}