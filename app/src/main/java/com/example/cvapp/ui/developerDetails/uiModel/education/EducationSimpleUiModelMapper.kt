package com.example.cvapp.ui.developerDetails.uiModel.education

import com.example.api.model.domain.education.Education
import com.example.cvapp.ui.DisplayedEducationTimeRangeFactory
import javax.inject.Inject

class EducationSimpleUiModelMapper @Inject constructor(
    private val timeRangeFactory: DisplayedEducationTimeRangeFactory
) {

    fun map(education: List<Education>?): List<EducationSimpleUiModel> = education?.map(::map) ?: emptyList()

    fun map(education: Education) = with(education){
        EducationSimpleUiModel(
            schoolName = schoolName,
            timeRange = timeRangeFactory.create(education),
            logoUrl = logoUrl
        )
    }
}