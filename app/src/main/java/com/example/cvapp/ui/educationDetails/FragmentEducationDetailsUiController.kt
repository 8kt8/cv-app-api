package com.example.cvapp.ui.educationDetails

import com.example.cvapp.ui.educationDetails.uiComponent.FragmentEducationDetailsUiComponent
import com.example.cvapp.ui.educationDetails.uiModel.EducationDetailsUiModel
import com.example.cvapp.ui.uiController.UiController
import javax.inject.Inject

class FragmentEducationDetailsUiController @Inject constructor()
    : UiController<FragmentEducationDetailsUiComponent>() {

    fun onBoundEducationDetails(uiModel: EducationDetailsUiModel){
        uiComponent.setData(uiModel)
    }
}