package com.example.cvapp.ui.developerDetails.uiModel.education

import com.example.api.model.domain.education.Degree
import javax.inject.Inject

class DegreeUiModelMapper @Inject constructor() {

    fun map(degrees: List<Degree>): List<DegreeUiModel> = degrees.map(::map)

    fun map(degree: Degree) = with(degree){
        DegreeUiModel(
            name = name,
            startYear = startYear,
            endYear = endYear,
            fieldOfStudy = fieldOfStudy
        )
    }
}