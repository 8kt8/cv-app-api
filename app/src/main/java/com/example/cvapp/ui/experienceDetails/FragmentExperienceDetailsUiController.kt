package com.example.cvapp.ui.experienceDetails

import com.example.cvapp.ui.experienceDetails.uiComponent.FragmentExperienceDetailsUiComponent
import com.example.cvapp.ui.experienceDetails.uiModel.ExperienceDetailsUiModel
import com.example.cvapp.ui.uiController.UiController
import javax.inject.Inject

class FragmentExperienceDetailsUiController @Inject constructor()
    : UiController<FragmentExperienceDetailsUiComponent>() {

    fun onBoundExperienceDetails(uiModel: ExperienceDetailsUiModel){
        uiComponent.setData(uiModel)
    }
}