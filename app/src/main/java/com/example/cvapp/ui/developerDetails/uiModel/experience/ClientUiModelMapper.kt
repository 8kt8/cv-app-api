package com.example.cvapp.ui.developerDetails.uiModel.experience
import com.example.api.model.domain.experience.Client
import javax.inject.Inject

class ClientUiModelMapper @Inject constructor() {

    fun map(client: Client?) = with(client){
        ClientUiModel(
            name = this?.name ?: "-",
            url = this?.url ?: "-",
            logoUrl = this?.logoUrl ?: ""
        )
    }
}