package com.example.cvapp.ui.developerDetails.uiModel

import com.example.cvapp.ui.developerDetails.uiModel.education.EducationSimpleUiModel
import com.example.cvapp.ui.developerDetails.uiModel.experience.ExperienceSimpleUiModel

data class DeveloperUiModel(
    val id: Int,
    val primaryTitle: String,
    val secondaryTitle: String,
    val photo_url: String,
    val dateOfBirth: String,
    val about: String,
    val contactUiModel: ContactUiModel?,
    val educationUiModels: List<EducationSimpleUiModel>,
    val experienceSimpleUiModels: List<ExperienceSimpleUiModel>,
    val skills: List<String>?,
    val programingLanguages: List<String>?,
    val trainings: List<String>?,
    val interests: List<String>?
)