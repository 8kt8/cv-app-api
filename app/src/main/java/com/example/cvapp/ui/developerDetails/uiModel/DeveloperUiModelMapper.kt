package com.example.cvapp.ui.developerDetails.uiModel

import com.example.api.model.domain.Developer
import com.example.cvapp.ui.developerDetails.uiModel.education.EducationSimpleUiModelMapper
import com.example.cvapp.ui.developerDetails.uiModel.experience.ExperienceSimpleUiModelMapper
import javax.inject.Inject

class DeveloperUiModelMapper @Inject constructor(
    private val contactUiModelMapper: ContactUiModelMapper,
    private val educationUiModelMapper: EducationSimpleUiModelMapper,
    private val experienceSimpleUiModelMapper: ExperienceSimpleUiModelMapper
) {

    fun map(developer: Developer) = with(developer){
        DeveloperUiModel(
            id = id,
            primaryTitle = "$firstName $lastName",
            secondaryTitle = experience?.last()?.positions?.last()?.name ?: "-",
            photo_url = photo_url,
            dateOfBirth = dateOfBirth,
            about = about,
            contactUiModel = contactUiModelMapper.map(contact),
            educationUiModels = educationUiModelMapper.map(education),
            experienceSimpleUiModels = experienceSimpleUiModelMapper.map(experience),
            skills = skills,
            programingLanguages = programingLanguages,
            trainings = trainings,
            interests = interests
        )
    }
}