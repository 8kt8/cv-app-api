package com.example.cvapp.ui.educationDetails.uiComponent

import android.widget.ArrayAdapter
import androidx.lifecycle.observe
import com.example.cvapp.R
import com.example.cvapp.ui.appBar.AppBarUiComponent
import com.example.cvapp.ui.educationDetails.FragmentEducationDetailsUiComponentHost
import com.example.cvapp.ui.educationDetails.FragmentEducationDetailsUiController
import com.example.cvapp.ui.educationDetails.uiModel.EducationDetailsUiModel
import com.example.cvapp.ui.extensions.loadCircleImage
import com.example.cvapp.ui.uiComponent.UiComponent
import javax.inject.Inject

class FragmentEducationDetailsUiComponent @Inject constructor(
    override val uiController: FragmentEducationDetailsUiController,
    private val appBarUiComponent: AppBarUiComponent
) : UiComponent<FragmentEducationDetailsUiComponentHost>() {

    override fun initUiController() = uiController.init(this)

    override fun init(host: FragmentEducationDetailsUiComponentHost) {
        super.init(host)
        appBarUiComponent.init(host)
        observeViewModel()
    }

    fun setData(uiModel: EducationDetailsUiModel) {
        host.binding.apply {
            componentAppBar.primaryTitle.text = uiModel.schoolName
            componentAppBar.secondaryTitle.text = uiModel.timeRange
            profileImage.loadCircleImage(uiModel.schoolLogoUrl)
            profileImage.setOnClickListener { appBarUiComponent.handleUrl(uiModel.schoolLogoUrl) }
            cardView.setOnClickListener { appBarUiComponent.handleUrl(uiModel.schoolLogoUrl) }
            listView.adapter = ArrayAdapter<String>(
                listView.context,
                R.layout.education_list_item,
                R.id.textView,
                uiModel.degreeUiModels
            )
        }
    }

    private fun observeViewModel() {
        host.viewModel.getEducation(host.developerId, host.educationId)
            .observe(host.lifecycleOwnerOfView, uiController::onBoundEducationDetails)
    }
}