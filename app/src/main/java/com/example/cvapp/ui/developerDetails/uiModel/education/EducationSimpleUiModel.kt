package com.example.cvapp.ui.developerDetails.uiModel.education

class EducationSimpleUiModel(
    val schoolName: String,
    val timeRange: String,
    val logoUrl: String
)