package com.example.cvapp.ui.developerDetails.uiModel.education

class EducationUiModel(
    val schoolName: String,
    val url: String,
    val logoUrl: String,
    val degreesUiModel: List<DegreeUiModel>
)