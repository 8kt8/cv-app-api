package com.example.cvapp.ui.experienceDetails.uiComponent

import androidx.lifecycle.observe
import com.example.cvapp.ui.appBar.AppBarUiComponent
import com.example.cvapp.ui.experienceDetails.FragmentExperienceDetailsUiController
import com.example.cvapp.ui.experienceDetails.list.ProjectListUiComponent
import com.example.cvapp.ui.experienceDetails.uiModel.ExperienceDetailsUiModel
import com.example.cvapp.ui.extensions.loadCircleImage
import com.example.cvapp.ui.uiComponent.UiComponent
import javax.inject.Inject

class FragmentExperienceDetailsUiComponent @Inject constructor(
    override val uiController: FragmentExperienceDetailsUiController,
    private val appBarUiComponent: AppBarUiComponent,
    private val projectListUiComponent: ProjectListUiComponent
) : UiComponent<FragmentExperienceDetailsUiComponentHost>() {

    override fun initUiController() = uiController.init(this)

    override fun init(host: FragmentExperienceDetailsUiComponentHost) {
        super.init(host)
        appBarUiComponent.init(host)
        projectListUiComponent.bindView(host.binding.recyclerView)
        observeViewModel()
    }

    fun setData(uiModel: ExperienceDetailsUiModel) {
        host.binding.apply {
            componentAppBar.primaryTitle.text = uiModel.companyName
            componentAppBar.secondaryTitle.text = uiModel.timeRange
            profileImage.loadCircleImage(uiModel.companyLogoUrl)
            projectListUiComponent.setData(uiModel.projectUiModels)
            profileImage.setOnClickListener { appBarUiComponent.handleUrl(uiModel.companyUrl) }
        }
        projectListUiComponent.onClickListener = appBarUiComponent::handleUrl
    }

    private fun observeViewModel() {
        host.viewModel.getExperience(host.developerId, host.experienceId)
            .observe(host.lifecycleOwnerOfView, uiController::onBoundExperienceDetails)
    }
}