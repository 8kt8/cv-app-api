package com.example.cvapp.ui.experienceDetails.list

import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.cvapp.databinding.ProjectItemBinding
import com.example.cvapp.ui.experienceDetails.uiModel.ProjectUiModel
import com.example.cvapp.ui.extensions.loadCircleImage
import com.mikepenz.fastadapter.binding.AbstractBindingItem

data class ProjectItem(
    val uiModel: ProjectUiModel
): AbstractBindingItem<ProjectItemBinding>() {

    override val type: Int = com.example.cvapp.R.id.beer_list_item

    override fun bindView(binding: ProjectItemBinding, payloads: List<Any>) {
        with(binding){
            textViewTitle.text = uiModel.positionName
            textViewSecondary.text = uiModel.timeRange
            textViewDescription.text = uiModel.description
            textViewProject.text = uiModel.companyName
            imageView.loadCircleImage(uiModel.logoUrl)
        }
    }

    override fun createBinding(inflater: LayoutInflater, parent: ViewGroup?): ProjectItemBinding =
        ProjectItemBinding.inflate(inflater, parent, false)

}