package com.example.cvapp.ui.developerDetails.uiModel.education

data class DegreeUiModel(
	val name : String,
	val fieldOfStudy : String,
	val startYear : String,
	val endYear : String
)