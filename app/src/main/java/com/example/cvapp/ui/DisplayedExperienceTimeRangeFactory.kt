package com.example.cvapp.ui

import com.example.api.model.domain.experience.Experience
import com.example.cvapp.ui.simpleListItem.SimpleListItemTimeRangeFactory
import javax.inject.Inject

class DisplayedExperienceTimeRangeFactory @Inject constructor(
    private val timeRangeFactory: SimpleListItemTimeRangeFactory
){

    fun create(experience: Experience) =
        "${createDisplayedStartDate(experience)} - ${createDisplayedEndDate(experience)}"

    private fun createDisplayedStartDate(experience: Experience) =
        with(experience.positions.first()){
            timeRangeFactory.createDisplayedStartDate(startDate)
    }

    private fun createDisplayedEndDate(experience: Experience) =
        with(experience.positions.last()){
            timeRangeFactory.createDisplayedEndDate(endDate)
    }
}