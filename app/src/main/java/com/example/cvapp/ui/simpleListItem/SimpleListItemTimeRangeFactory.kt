package com.example.cvapp.ui.simpleListItem

import com.example.cvapp.common.converter.DateTimeConverter
import javax.inject.Inject

class SimpleListItemTimeRangeFactory @Inject constructor(
    private val dateTimeConverter: DateTimeConverter
) {

    fun createDisplayedStartDate(startDate: String): String {
        return dateTimeConverter.convert(startDate)?.let {
            "${it.toString("MMM")} ${it.year}"
        } ?: ""
    }

    fun createDisplayedEndDate(endDate: String): String {
        if(isPresentTime(endDate)){
            return endDate
        }
        return dateTimeConverter.convert(endDate)?.let {
            "${it.toString("MMM")} ${it.year}"
        } ?: ""
    }

    private fun isPresentTime(endDate: String) = endDate == "Present"
}