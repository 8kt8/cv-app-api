package com.example.cvapp.ui.experienceDetails.list


import com.example.cvapp.ui.experienceDetails.uiModel.ProjectUiModel
import javax.inject.Inject

class ProjectItemMapper @Inject constructor() {

    fun toItems(projects: List<ProjectUiModel>): List<ProjectItem> = projects.map(::toItem)

    fun toItem(uiModel: ProjectUiModel): ProjectItem = ProjectItem(uiModel).apply {
        identifier = uiModel.hashCode().toLong()
    }
}