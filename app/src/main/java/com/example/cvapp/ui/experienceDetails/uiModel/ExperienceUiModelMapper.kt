package com.example.cvapp.ui.experienceDetails.uiModel

import com.example.api.model.domain.experience.Experience
import com.example.api.model.domain.experience.Position
import com.example.api.model.domain.experience.Project
import com.example.cvapp.ui.simpleListItem.SimpleListItemTimeRangeFactory
import javax.inject.Inject

class ExperienceUiModelMapper @Inject constructor(
    private val timeRangeFactory: SimpleListItemTimeRangeFactory
) {

    fun map(experience: Experience): List<ProjectUiModel> = experience.positions
        .asReversed()
        .flatMap { position ->
            position.projects
                .asReversed()
                .map { project ->
                    map(experience.logoUrl, position, project)
                }
        }

    fun map(companyLogoUrl: String, position: Position, project: Project) =
        ProjectUiModel(
            positionName = position.name,
            companyName = project.client.name,
            timeRange = "${createDisplayedStartDate(project)} - ${createDisplayedEndDate(project)}",
            logoUrl = getLogoUrl(project.client.logoUrl, companyLogoUrl),
            companyUrl = project.client.url,
            description = project.description
        )

    private fun getLogoUrl(logoUrl: String, companyLogoUrl: String) =
        if(logoUrl.isBlank()) companyLogoUrl else logoUrl

    private fun createDisplayedStartDate(project: Project) =
        timeRangeFactory.createDisplayedStartDate(project.startDate)

    private fun createDisplayedEndDate(project: Project) =
        timeRangeFactory.createDisplayedEndDate(project.endDate)
}