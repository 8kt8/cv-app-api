package com.example.cvapp.ui.developerDetails

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import androidx.navigation.NavDirections
import androidx.navigation.fragment.findNavController
import com.example.api.common.fastLazy
import com.example.cvapp.R
import com.example.cvapp.common.getPresentationComponent
import com.example.cvapp.databinding.ComponentAppBarBinding
import com.example.cvapp.databinding.FragmentDevelopersDetailsBinding
import com.example.cvapp.ui.developerContact.DeveloperContactViewModel
import com.example.cvapp.ui.developerDetails.uiComponent.FragmentDeveloperDetailsUiComponent
import com.example.cvapp.ui.developerDetails.uiComponent.FragmentDeveloperDetailsUiComponentHost
import com.example.cvapp.ui.main.BindingFragment
import javax.inject.Inject

class FragmentDeveloperDetails : BindingFragment<FragmentDevelopersDetailsBinding>(),
    FragmentDeveloperDetailsUiComponentHost {

    @Inject
    lateinit var uiComponent: FragmentDeveloperDetailsUiComponent

    override fun onAttach(context: Context) {
        super.onAttach(context)
        getPresentationComponent().inject(this)
    }

    override val viewModel
            by fastLazy { forFragment<DeveloperDetailsViewModel>() }

    override val viewModelContact: DeveloperContactViewModel
            by fastLazy { forActivity<DeveloperContactViewModel>() }

    override val imageVieAvatarIcon: ImageView
        get() = binding.profileImage

    override val componentAppBarBinding: ComponentAppBarBinding
        get() = binding.componentAppBar

    override fun isAvatarShown(): Boolean = viewModel.isAvatarShown

    override fun setIsAvatarShown(isShown: Boolean) {
        viewModel.isAvatarShown = isShown
    }

    override val layoutId: Int = R.layout.fragment_developers_details

    override fun navigate(navDirections: NavDirections) {
        findNavController().navigate(navDirections)
    }

    override fun startIntent(intent: Intent, chooserTitle: String) {
        requireActivity().startActivity(Intent.createChooser(intent, chooserTitle))
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        uiComponent.init(this)
    }
}