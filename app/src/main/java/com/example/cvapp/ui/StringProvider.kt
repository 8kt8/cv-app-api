package com.example.cvapp.ui

import android.content.Context
import androidx.annotation.StringRes
import com.example.api.di.ApplicationContext
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class StringProvider @Inject constructor(
    @ApplicationContext private val context: Context
) {
    fun provideString(@StringRes resId: Int, vararg params: Any): String {
        return context.getString(resId, *params)
    }

    fun providePlural(resId: Int, value: Int): String {
        return context.resources.getQuantityString(resId, value)
    }

    fun provideQuantityString(resId: Int, quantity: Int, vararg params: Any): String {
        return context.resources.getQuantityString(resId, quantity, *params)
    }
}