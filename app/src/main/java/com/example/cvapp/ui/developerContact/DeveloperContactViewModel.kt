package com.example.cvapp.ui.developerContact

import com.example.cvapp.ui.developerDetails.uiModel.ContactUiModel
import com.example.cvapp.ui.main.BaseViewModel
import javax.inject.Inject

class DeveloperContactViewModel @Inject constructor(): BaseViewModel() {

    var contact: ContactUiModel? = null
}