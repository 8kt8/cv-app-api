package com.example.cvapp.ui.educationDetails.uiModel

import com.example.api.model.domain.education.Education
import com.example.cvapp.ui.DisplayedEducationTimeRangeFactory
import javax.inject.Inject

class EducationDetailsUiModelMapper @Inject constructor(
    private val timeRangeFactory: DisplayedEducationTimeRangeFactory,
    private val degreeUiModelMapper: DegreeUiModelMapper
) {

    fun map(education: Education) =
        EducationDetailsUiModel(
            schoolName = education.schoolName,
            timeRange = timeRangeFactory.create(education),
            schoolLogoUrl = education.logoUrl,
            schoolUrl = education.url,
            degreeUiModels = degreeUiModelMapper.map(education)
        )
}

