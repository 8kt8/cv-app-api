package com.example.cvapp.ui.educationDetails.uiModel

data class EducationDetailsUiModel(
    val schoolName: String,
    val timeRange: String,
    val schoolLogoUrl: String,
    val schoolUrl: String,
    val degreeUiModels: List<String>
)