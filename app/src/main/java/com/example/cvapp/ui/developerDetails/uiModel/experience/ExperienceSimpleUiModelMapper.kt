package com.example.cvapp.ui.developerDetails.uiModel.experience

import com.example.api.model.domain.experience.Experience
import com.example.cvapp.ui.DisplayedExperienceTimeRangeFactory
import javax.inject.Inject

class ExperienceSimpleUiModelMapper @Inject constructor(
    private val timeRangeFactory: DisplayedExperienceTimeRangeFactory
) {

    fun map(experience: List<Experience>?): List<ExperienceSimpleUiModel> = experience?.map(::map) ?: emptyList()

    fun map(experience: Experience) = with(experience){
        ExperienceSimpleUiModel(
            companyName = companyName,
            timeRange = timeRangeFactory.create(experience),
            logoUrl = logoUrl
        )
    }
}