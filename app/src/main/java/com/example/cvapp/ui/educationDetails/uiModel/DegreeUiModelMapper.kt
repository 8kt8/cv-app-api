package com.example.cvapp.ui.educationDetails.uiModel

import com.example.api.model.domain.education.Education
import javax.inject.Inject

class DegreeUiModelMapper @Inject constructor() {

    fun map(education: Education) = education.degrees
        .asReversed()
        .map { "${it.name} ${it.fieldOfStudy} ${it.startYear}- ${it.endYear}" }
}