package com.example.cvapp.ui.developerDetails.uiComponent

import android.view.View
import android.view.ViewGroup
import com.example.cvapp.databinding.SimpleListItemBinding
import com.example.cvapp.ui.developerDetails.uiModel.experience.ExperienceSimpleUiModel
import com.example.cvapp.ui.extensions.loadCircleImage
import com.example.cvapp.ui.simpleListItem.SimpleListItemFactory
import javax.inject.Inject

class DeveloperDetailsExperienceUiComponent @Inject constructor(
    private val simpleListItemFactory: SimpleListItemFactory
) {

    private var experienceViews: ArrayList<SimpleListItemBinding> = arrayListOf()

    var onClickListener: (Int, View) -> Unit = { _, _ ->}

    fun setData(experienceUiModels: List<ExperienceSimpleUiModel>, rootLayout: ViewGroup){
        if (experienceUiModels.size != experienceViews.size) {
            experienceViews.clear()
        }
        experienceUiModels
            .asReversed()
            .forEachIndexed { index, uiModel ->
                if (experienceViews.size < index + 1) {
                    val view = simpleListItemFactory.create(rootLayout)
                    experienceViews.add(view)
                    view.root.setOnClickListener {
                        onClickListener(index, it)
                    }
                }
                with(experienceViews[index]) {
                    imageView.loadCircleImage(uiModel.logoUrl)
                    textViewTitle.text = uiModel.companyName
                    textViewSecondary.text = uiModel.timeRange
                    if (root.parent != null) {
                        (root.parent as ViewGroup).removeView(root)
                    }
                    rootLayout.addView(root)
                }
            }
    }
}