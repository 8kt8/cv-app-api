package com.example.cvapp.ui.developerDetails.uiModel

import com.example.api.model.domain.Contact
import com.example.cvapp.ui.title.SpannableTitleFactory
import javax.inject.Inject

class ContactUiModelMapper @Inject constructor(
    private val spannableTitleFactory: SpannableTitleFactory
){

    fun map(contact: Contact?): ContactUiModel = with(contact){
        ContactUiModel(
            email = this?.email ?: "",
            phoneNumber = this?.phoneNumber ?: "",
            linkedinUrl = this?.linkedinUrl ?: ""
        )
    }
}