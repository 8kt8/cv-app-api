package com.example.cvapp.ui.uiComponent

import com.example.cvapp.ui.uiController.UiController
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

abstract class UiComponent<Host : UiComponentHost> {

    private val disposables = CompositeDisposable()

    protected abstract val uiController: UiController<*>

    protected abstract fun initUiController()

    lateinit var host: Host
        private set

    open fun init(host: Host) {
        this.host = host
        initUiController()
    }

    protected fun onDestroy() {
        disposables.clear()
    }

    protected fun Disposable.remember() {
        disposables.add(this)
    }
}
