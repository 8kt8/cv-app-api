package com.example.cvapp.ui.developerDetails

import com.example.cvapp.ui.developerDetails.uiComponent.FragmentDeveloperDetailsUiComponent
import com.example.cvapp.ui.developerDetails.uiModel.DeveloperUiModel
import com.example.cvapp.ui.uiController.UiController
import javax.inject.Inject

class FragmentDeveloperDetailsUiController @Inject constructor()
    : UiController<FragmentDeveloperDetailsUiComponent>() {

    override fun init(uiComponent: FragmentDeveloperDetailsUiComponent) {
        super.init(uiComponent)
        uiComponent.showLoading()
    }

    fun onBoundDeveloperDetails(uiModel: DeveloperUiModel){
        uiComponent.hideLoading()
        uiComponent.setData(uiModel)
    }

    fun onExperienceItemClick(developerId: Int, index: Int){
        val navDirections =
            FragmentDeveloperDetailsDirections
                .actionFragmentDeveloperDetailsToFragmentExperienceDetails(developerId, index)
        uiComponent.host.navigate(navDirections)
    }

    fun onEducationItemClick(developerId: Int, index: Int){
        val navDirections =
            FragmentDeveloperDetailsDirections
                .actionFragmentDeveloperDetailsToFragmentEducationDetails(developerId, index)
        uiComponent.host.navigate(navDirections)
    }
}