package com.example.cvapp.ui.experienceDetails

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import androidx.navigation.fragment.navArgs
import com.example.api.common.fastLazy
import com.example.cvapp.R
import com.example.cvapp.common.getPresentationComponent
import com.example.cvapp.databinding.ComponentAppBarBinding
import com.example.cvapp.databinding.FragmentExperienceDetailsBinding
import com.example.cvapp.ui.developerContact.DeveloperContactViewModel
import com.example.cvapp.ui.experienceDetails.uiComponent.FragmentExperienceDetailsUiComponent
import com.example.cvapp.ui.experienceDetails.uiComponent.FragmentExperienceDetailsUiComponentHost
import com.example.cvapp.ui.main.BindingFragment
import javax.inject.Inject

class FragmentExperienceDetails: BindingFragment<FragmentExperienceDetailsBinding>(),
    FragmentExperienceDetailsUiComponentHost {

    @Inject
    lateinit var uiComponent: FragmentExperienceDetailsUiComponent

    override fun onAttach(context: Context) {
        super.onAttach(context)
        getPresentationComponent().inject(this)
    }

    override val viewModel
            by fastLazy { forFragment<ExperienceDetailsViewModel>() }

    override val viewModelContact: DeveloperContactViewModel
            by fastLazy { forActivity<DeveloperContactViewModel>() }

    override val layoutId: Int = R.layout.fragment_experience_details

    override val imageVieAvatarIcon: ImageView
        get() = binding.profileImage

    override val componentAppBarBinding: ComponentAppBarBinding
        get() = binding.componentAppBar

    override fun isAvatarShown(): Boolean = viewModel.isAvatarShown

    override fun setIsAvatarShown(isShown: Boolean) {
        viewModel.isAvatarShown = isShown
    }

    override fun startIntent(intent: Intent, chooserTitle: String) {
        requireActivity().startActivity(Intent.createChooser(intent, chooserTitle))
    }

    private val args: FragmentExperienceDetailsArgs by navArgs()

    override val developerId: Int
        get() = args.developerId

    override val experienceId: Int
        get() = args.experienceId

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        uiComponent.init(this)
    }
}