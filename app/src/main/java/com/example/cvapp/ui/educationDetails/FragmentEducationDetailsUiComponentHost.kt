package com.example.cvapp.ui.educationDetails

import com.example.cvapp.databinding.FragmentEducationDetailsBinding
import com.example.cvapp.ui.appBar.AppBarUiComponentHost

interface FragmentEducationDetailsUiComponentHost : AppBarUiComponentHost {

    val viewModel: EducationDetailsViewModel
    val binding: FragmentEducationDetailsBinding
    val developerId: Int
    val educationId: Int
}