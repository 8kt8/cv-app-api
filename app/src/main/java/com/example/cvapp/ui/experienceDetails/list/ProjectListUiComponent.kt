package com.example.cvapp.ui.experienceDetails.list

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.cvapp.ui.experienceDetails.uiModel.ProjectUiModel
import com.mikepenz.fastadapter.adapters.FastItemAdapter
import com.mikepenz.fastadapter.diff.FastAdapterDiffUtil
import javax.inject.Inject

class ProjectListUiComponent @Inject constructor(
    private val projectItemMapper: ProjectItemMapper
) {

    private lateinit var _recyclerView: RecyclerView

    private val fastItemAdapter = FastItemAdapter<ProjectItem>()

    init {
        fastItemAdapter.onClickListener = {
            _,_,item,_ ->
            onClickListener(item.uiModel.companyUrl)
            true
        }
    }

    var onClickListener: (String)-> Unit = {}

    fun bindView(recyclerView: RecyclerView) {
        _recyclerView = recyclerView.apply {
            adapter = fastItemAdapter
            layoutManager = LinearLayoutManager(context)
        }
    }

    fun setData(projects: List<ProjectUiModel>){
        val newItems = projectItemMapper.toItems(projects)
        FastAdapterDiffUtil[fastItemAdapter.itemAdapter] = newItems
    }
}