package com.example.cvapp.ui.extensions

import android.view.View
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.example.cvapp.R

fun ImageView.loadCircleImage(imageUrl: String){
    Glide
        .with(context)
        .load(imageUrl)
        .circleCrop()
        .placeholder(R.drawable.user_avatar)
        .into(this)
}

fun View.show() {
    visibility = View.VISIBLE
}

fun View.hide() {
    visibility = View.GONE
}

fun View.showIf(show: Boolean) {
   if(show) show() else hide()
}