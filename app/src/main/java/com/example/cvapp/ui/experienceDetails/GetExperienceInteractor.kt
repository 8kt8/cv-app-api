package com.example.cvapp.ui.experienceDetails

import com.example.api.repository.DeveloperRepository
import javax.inject.Inject

class GetExperienceInteractor @Inject constructor(
    private val developerRepository: DeveloperRepository
) {

    fun getExperience(developerId: Int, experienceId: Int) =
        developerRepository.getById(developerId)
            .map { it.experience }
            .map { it.asReversed()[experienceId] }
}