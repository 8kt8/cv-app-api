package com.example.cvapp.ui.educationDetails

import androidx.lifecycle.LiveDataReactiveStreams
import com.example.cvapp.ui.educationDetails.uiModel.EducationDetailsUiModelMapper
import com.example.cvapp.ui.main.BaseViewModel
import javax.inject.Inject

class EducationDetailsViewModel @Inject constructor(
    private val getEducationInteractor: GetEducationInteractor,
    private val educationDetailsUiModelMapper: EducationDetailsUiModelMapper
): BaseViewModel() {

    var isAvatarShown: Boolean = true

    fun getEducation(developerId: Int, id: Int) = LiveDataReactiveStreams.fromPublisher(
        getEducationInteractor.getExperience(developerId, id)
            .map(educationDetailsUiModelMapper::map)
            .distinctUntilChanged()
    )
}