package com.example.cvapp.ui.experienceDetails.uiComponent

import com.example.cvapp.databinding.FragmentExperienceDetailsBinding
import com.example.cvapp.ui.appBar.AppBarUiComponentHost
import com.example.cvapp.ui.experienceDetails.ExperienceDetailsViewModel

interface FragmentExperienceDetailsUiComponentHost : AppBarUiComponentHost {

    val viewModel: ExperienceDetailsViewModel
    val binding: FragmentExperienceDetailsBinding
    val developerId: Int
    val experienceId: Int
}