package com.example.cvapp.ui.developerDetails.uiModel

data class ContactUiModel(
    val email: String,
    val phoneNumber: String,
    val linkedinUrl: String
)