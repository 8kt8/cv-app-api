package com.example.cvapp.ui.experienceDetails.di

import androidx.lifecycle.ViewModel
import com.example.cvapp.di.ViewModelKey
import com.example.cvapp.ui.experienceDetails.ExperienceDetailsViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ExperienceDetailsModule {
    @Binds
    @IntoMap
    @ViewModelKey(ExperienceDetailsViewModel::class)
    internal abstract fun bindExperienceDetailsViewModel(viewModel: ExperienceDetailsViewModel): ViewModel

}