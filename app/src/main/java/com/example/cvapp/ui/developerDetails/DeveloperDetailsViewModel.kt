package com.example.cvapp.ui.developerDetails

import androidx.lifecycle.LiveData
import androidx.lifecycle.LiveDataReactiveStreams
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations.switchMap
import com.example.api.repository.interactor.GetDeveloperByIdInteractor
import com.example.cvapp.ui.developerDetails.uiModel.DeveloperUiModel
import com.example.cvapp.ui.developerDetails.uiModel.DeveloperUiModelMapper
import com.example.cvapp.ui.main.BaseViewModel
import javax.inject.Inject

class DeveloperDetailsViewModel @Inject constructor(
    private val getDeveloperByIdInteractor: GetDeveloperByIdInteractor,
    private val developerUiModelMapper: DeveloperUiModelMapper
): BaseViewModel() {

    var isAvatarShown = true

    private val developerId: MutableLiveData<Int> = MutableLiveData()

    fun setDeveloperId(id: Int) {
        developerId.value = id
    }

    val developer: LiveData<DeveloperUiModel> = switchMap(developerId){
        LiveDataReactiveStreams.fromPublisher(
            getDeveloperByIdInteractor.getById(it)
                .map(developerUiModelMapper::map)
                .distinctUntilChanged()
        )
    }

    private fun getDeveloper(id: Int) = LiveDataReactiveStreams.fromPublisher(
        getDeveloperByIdInteractor.getById(id)
            .map(developerUiModelMapper::map)
            .distinctUntilChanged()
    )
}