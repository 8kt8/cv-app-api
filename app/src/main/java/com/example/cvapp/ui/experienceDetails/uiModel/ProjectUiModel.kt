package com.example.cvapp.ui.experienceDetails.uiModel

class ProjectUiModel(
    val positionName: String,
    val companyName: String,
    val timeRange: String,
    val logoUrl: String,
    val companyUrl: String,
    val description: String
)