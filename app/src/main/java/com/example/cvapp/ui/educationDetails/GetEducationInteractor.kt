package com.example.cvapp.ui.educationDetails

import com.example.api.repository.DeveloperRepository
import javax.inject.Inject

class GetEducationInteractor @Inject constructor(
    private val developerRepository: DeveloperRepository
) {

    fun getExperience(developerId: Int, educationId: Int) =
        developerRepository.getById(developerId)
            .map { it.education }
            .map { it.asReversed()[educationId] }
}