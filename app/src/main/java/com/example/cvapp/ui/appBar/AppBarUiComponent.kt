package com.example.cvapp.ui.appBar

import com.example.cvapp.ui.uiComponent.UiComponent
import com.google.android.material.appbar.AppBarLayout
import javax.inject.Inject

class AppBarUiComponent @Inject constructor(
    override val uiController: AppBarUiController
): UiComponent<AppBarUiComponentHost>(),
    AppBarLayout.OnOffsetChangedListener {

    override fun initUiController() = uiController.init(this)

    override fun init(host: AppBarUiComponentHost) {
        super.init(host)
        host.componentAppBarBinding.appbar.addOnOffsetChangedListener(this)
        uiController.onRestoreIconState()
        initBinding()
    }

    private fun initBinding() {
        host.componentAppBarBinding.apply {
            imageGmail.setOnClickListener { uiController.onGmailClick() }
            imageContact.setOnClickListener { uiController.onContactClick() }
            imageLinkedin.setOnClickListener { uiController.onUrlClick() }
        }
    }

    override fun onOffsetChanged(appBarLayout: AppBarLayout, i: Int) {
        uiController.onOffsetChanged(appBarLayout, i)
    }

    fun handleUrl(url: String) = uiController.onUrlHandle(url)

    fun showImage(duration: Long = 200){
        animateProfileImage(0f, 0f, duration)
    }

    fun hideImage(duration: Long = 200) = animateProfileImage(1f, 1f, duration)


    private fun animateProfileImage(scaleY: Float, scaleX: Float, duration: Long = 200){
        host.imageVieAvatarIcon.animate()
            .scaleY(scaleY).scaleX(scaleX)
            .setDuration(duration)
            .start()
    }
}