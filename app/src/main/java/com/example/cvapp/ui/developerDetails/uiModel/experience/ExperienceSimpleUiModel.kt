package com.example.cvapp.ui.developerDetails.uiModel.experience

data class ExperienceSimpleUiModel(
    val companyName: String,
    val timeRange: String,
    val logoUrl: String
)