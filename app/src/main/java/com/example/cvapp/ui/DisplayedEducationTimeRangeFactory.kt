package com.example.cvapp.ui

import com.example.api.model.domain.education.Education
import javax.inject.Inject

class DisplayedEducationTimeRangeFactory @Inject constructor(){

    fun create(education: Education) =
        "${createDisplayedStartYear(education)} - ${createDisplayedEndYear(education)}"


    private fun createDisplayedStartYear(education: Education) =
        education.degrees.first().startYear

    private fun createDisplayedEndYear(education: Education) =
        education.degrees.last().endYear
}