package com.example.cvapp.ui.developerDetails.uiComponent

import androidx.navigation.NavDirections
import com.example.cvapp.databinding.FragmentDevelopersDetailsBinding
import com.example.cvapp.ui.appBar.AppBarUiComponentHost
import com.example.cvapp.ui.developerDetails.DeveloperDetailsViewModel

interface FragmentDeveloperDetailsUiComponentHost: AppBarUiComponentHost {

    val viewModel: DeveloperDetailsViewModel
    val binding: FragmentDevelopersDetailsBinding
    fun navigate(navDirections: NavDirections)
}