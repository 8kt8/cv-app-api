package com.example.cvapp.ui.simpleListItem

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.cvapp.databinding.SimpleListItemBinding
import javax.inject.Inject

class SimpleListItemFactory @Inject constructor(
    private val context: Context
) {

    fun create(parentView: ViewGroup) = SimpleListItemBinding
        .inflate(LayoutInflater.from(context), parentView, false)

}