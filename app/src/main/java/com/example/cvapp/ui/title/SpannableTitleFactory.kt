package com.example.cvapp.ui.title

import android.content.Context
import android.graphics.Typeface
import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.style.ForegroundColorSpan
import android.text.style.StyleSpan
import androidx.core.content.ContextCompat
import com.example.cvapp.R
import javax.inject.Inject

class SpannableTitleFactory @Inject constructor(
    private val context: Context
) {

    fun create(title: String): SpannableStringBuilder {
        val str = SpannableStringBuilder(title)
        val fcs = ForegroundColorSpan(ContextCompat.getColor(context, R.color.colorPrimary))
        val bss = StyleSpan(Typeface.BOLD)
        str.setSpan(fcs, 0, title.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        str.setSpan(bss, 0, title.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        return str
    }

}