package com.example.cvapp.ui.main

import android.os.Bundle
import com.example.api.repository.interactor.RefreshDeveloperInteractor
import com.example.api.rx.CoreSchedulers
import com.example.cvapp.R
import com.example.cvapp.di.DependencyInjectionActivity
import com.orhanobut.logger.Logger
import javax.inject.Inject

class MainActivity : DependencyInjectionActivity() {

    @Inject
    lateinit var coreSchedulers: CoreSchedulers

    @Inject
    lateinit var refreshDeveloperInteractor: RefreshDeveloperInteractor

    override fun onCreate(savedInstanceState: Bundle?) {
        presentationComponent.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
        refreshDeveloperInteractor.refresh().subscribe({
            Logger.e("!!!!!!! success")
        }, {
            Logger.e("!!!!!!! error ${it.message}")
        })
    }
}