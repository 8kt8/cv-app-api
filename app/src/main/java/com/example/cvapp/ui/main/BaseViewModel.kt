package com.example.cvapp.ui.main

import androidx.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import javax.inject.Inject

open class BaseViewModel @Inject internal constructor(): ViewModel() {

    protected val disposables = CompositeDisposable()

    protected fun Disposable.remember() {
        disposables.add(this)
    }

    override fun onCleared() {
        disposables.clear()
        super.onCleared()
    }
}