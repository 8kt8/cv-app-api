package com.example.cvapp.ui.experienceDetails.uiModel

data class ExperienceDetailsUiModel(
    val companyName: String,
    val timeRange: String,
    val companyLogoUrl: String,
    val companyUrl: String,
    val projectUiModels: List<ProjectUiModel>
)