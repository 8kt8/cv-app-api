package com.example.cvapp.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.ViewModelProvider
import com.example.cvapp.ui.uiComponent.UiComponentHost

abstract class BindingFragment<Binding : ViewDataBinding> : BaseFragment(), UiComponentHost {

    private lateinit var _binding: Binding

    val binding: Binding
        get() = _binding

    override val lifecycleOwnerOfView: LifecycleOwner
        get() = viewLifecycleOwner

    protected abstract val layoutId: Int

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return DataBindingUtil.inflate<Binding>(inflater, layoutId, container, false)
            .apply {
                lifecycleOwner = viewLifecycleOwner
                _binding = this
            }.root

    }

    protected inline fun <reified ViewModel : BaseViewModel> forActivity() =
        ViewModelProvider(requireActivity(), viewModelFactory).get(ViewModel::class.java)


    protected inline fun <reified ViewModel : BaseViewModel> forFragment(
        owner: Fragment = this
    ) = ViewModelProvider(owner, viewModelFactory).get(ViewModel::class.java)

}