package com.example.cvapp.ui.experienceDetails

import androidx.lifecycle.LiveDataReactiveStreams
import com.example.cvapp.ui.experienceDetails.uiModel.ExperienceDetailsUiModelMapper
import com.example.cvapp.ui.main.BaseViewModel
import javax.inject.Inject

class ExperienceDetailsViewModel @Inject constructor(
    private val getExperienceInteractor: GetExperienceInteractor,
    private val experienceDetailsUiModelMapper: ExperienceDetailsUiModelMapper
): BaseViewModel() {

    var isAvatarShown: Boolean = true

    fun getExperience(developerId: Int, id: Int) = LiveDataReactiveStreams.fromPublisher(
        getExperienceInteractor.getExperience(developerId, id)
            .map(experienceDetailsUiModelMapper::map)
            .distinctUntilChanged()
    )
}