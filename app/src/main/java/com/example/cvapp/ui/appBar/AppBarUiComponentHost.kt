package com.example.cvapp.ui.appBar

import android.content.Intent
import android.widget.ImageView
import com.example.cvapp.databinding.ComponentAppBarBinding
import com.example.cvapp.ui.developerContact.DeveloperContactViewModel
import com.example.cvapp.ui.uiComponent.UiComponentHost

interface AppBarUiComponentHost: UiComponentHost {

    val componentAppBarBinding: ComponentAppBarBinding
    val imageVieAvatarIcon: ImageView
    val viewModelContact: DeveloperContactViewModel
    fun isAvatarShown(): Boolean
    fun setIsAvatarShown(isShown: Boolean)
    fun startIntent(intent: Intent, chooserTitle: String)
}