package com.example.cvapp.ui.uiController

import com.example.cvapp.ui.uiComponent.UiComponent

abstract class UiController<Component : UiComponent<*>> {

    protected lateinit var uiComponent: Component
        private set

    open fun init(uiComponent: Component) {
        this.uiComponent = uiComponent
    }
}
