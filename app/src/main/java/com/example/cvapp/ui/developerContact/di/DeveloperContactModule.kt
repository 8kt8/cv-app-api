package com.example.cvapp.ui.developerContact.di

import androidx.lifecycle.ViewModel
import com.example.cvapp.di.ViewModelKey
import com.example.cvapp.ui.developerContact.DeveloperContactViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class DeveloperContactModule {

    @Binds
    @IntoMap
    @ViewModelKey(DeveloperContactViewModel::class)
    internal abstract fun bindDeveloperContactViewModel(userViewModel: DeveloperContactViewModel): ViewModel
}