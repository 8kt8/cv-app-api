package com.example.cvapp.ui.developerDetails.uiComponent

import android.view.View
import android.view.ViewGroup
import com.example.cvapp.databinding.SimpleListItemBinding
import com.example.cvapp.ui.developerDetails.uiModel.education.EducationSimpleUiModel
import com.example.cvapp.ui.extensions.loadCircleImage
import com.example.cvapp.ui.simpleListItem.SimpleListItemFactory
import javax.inject.Inject

class DeveloperDetailsEducationUiComponent @Inject constructor(
    private val simpleListItemFactory: SimpleListItemFactory
) {

    private var educationViews: ArrayList<SimpleListItemBinding> = arrayListOf()

    var onClickListener: (Int, View) -> Unit = { _, _ ->}

    fun setData(educationUiModels: List<EducationSimpleUiModel>, rootLayout: ViewGroup) {
        if (educationUiModels.size != educationViews.size) {
            educationViews.clear()
        }
        educationUiModels.asReversed()
            .forEachIndexed { index, uiModel ->
                if (educationViews.size < index + 1) {
                    val view = simpleListItemFactory.create(rootLayout)
                    educationViews.add(view)
                    view.root.setOnClickListener {
                        onClickListener(index, it)
                    }
                }

                with(educationViews[index]) {
                    imageView.loadCircleImage(uiModel.logoUrl)
                    textViewTitle.text = uiModel.schoolName
                    textViewSecondary.text = uiModel.timeRange
                    if (root.parent != null) {
                        (root.parent as ViewGroup).removeView(root)
                    }
                    rootLayout.addView(root)
                }
            }
    }
}