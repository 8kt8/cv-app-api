package com.example.cvapp.ui.uiComponent

import androidx.lifecycle.LifecycleOwner

interface UiComponentHost {

    val lifecycleOwnerOfView: LifecycleOwner
}
