package com.example.cvapp.ui.developerDetails.uiComponent

import android.widget.ArrayAdapter
import androidx.lifecycle.observe
import com.example.cvapp.R
import com.example.cvapp.databinding.FragmentDevelopersDetailsBinding
import com.example.cvapp.ui.appBar.AppBarUiComponent
import com.example.cvapp.ui.developerDetails.FragmentDeveloperDetailsUiController
import com.example.cvapp.ui.developerDetails.uiModel.DeveloperUiModel
import com.example.cvapp.ui.extensions.hide
import com.example.cvapp.ui.extensions.loadCircleImage
import com.example.cvapp.ui.extensions.show
import com.example.cvapp.ui.uiComponent.UiComponent
import javax.inject.Inject

class FragmentDeveloperDetailsUiComponent @Inject constructor(
    override val uiController: FragmentDeveloperDetailsUiController,
    private val appBarUiComponent: AppBarUiComponent,
    private val experienceUiComponent: DeveloperDetailsExperienceUiComponent,
    private val educationUiComponent: DeveloperDetailsEducationUiComponent
) : UiComponent<FragmentDeveloperDetailsUiComponentHost>() {

    override fun initUiController() = uiController.init(this)

    override fun init(host: FragmentDeveloperDetailsUiComponentHost) {
        super.init(host)
        appBarUiComponent.init(host)
        host.viewModel.setDeveloperId(DEVELOPER_ID)
        observeViewModel()
        showLoading()
    }

    fun showLoading() {
        host.binding.apply {
           progressBar.show()
           contentRootLayout.hide()
        }
    }

    fun hideLoading() {
        host.binding.apply {
            progressBar.hide()
            contentRootLayout.show()
        }
    }

    fun setData(uiModel: DeveloperUiModel) {
        host.viewModelContact.contact = uiModel.contactUiModel
        host.binding.apply {
            componentAppBar.primaryTitle.text = uiModel.primaryTitle
            componentAppBar.secondaryTitle.text = uiModel.secondaryTitle
            textViewAbout.text = uiModel.about
            profileImage.loadCircleImage(uiModel.photo_url)
            experienceUiComponent.setData(uiModel.experienceSimpleUiModels, experienceRootLayout)
            experienceUiComponent.onClickListener = { index, _ ->
                uiController.onExperienceItemClick(DEVELOPER_ID, index)
            }
            educationUiComponent.setData(uiModel.educationUiModels, educationRootLayout)
            educationUiComponent.onClickListener = { index, _ ->
                uiController.onEducationItemClick(DEVELOPER_ID, index)
            }
        }
        setSkillsData(host.binding, uiModel.skills)
        setInterestsData(host.binding, uiModel.interests)
        hideLoading()
    }

    private fun setSkillsData(binding: FragmentDevelopersDetailsBinding, items: List<String>?) = items?.let {
        binding.cardViewSkills.show()
        binding.gridViewSkills.adapter = ArrayAdapter<String>(binding.gridViewSkills.context, R.layout.array_list_item, R.id.textView, it)
    } ?: binding.cardViewSkills.hide()

    private fun setInterestsData(binding: FragmentDevelopersDetailsBinding, items: List<String>?) = items?.let {
        binding.cardViewInterests.show()
        binding.gridViewInterest.adapter = ArrayAdapter<String>(binding.gridViewInterest.context, R.layout.array_list_item, R.id.textView, it)
    } ?: binding.cardViewInterests.hide()


    private fun observeViewModel() {
        host.viewModel.developer
            .observe(host.lifecycleOwnerOfView, uiController::onBoundDeveloperDetails)
    }

    companion object{
        private const val DEVELOPER_ID = 1
    }
}