package com.example.cvapp.ui.developerDetails.di

import androidx.lifecycle.ViewModel
import com.example.cvapp.di.ViewModelKey
import com.example.cvapp.ui.developerDetails.DeveloperDetailsViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class DeveloperDetailsModule {

    @Binds
    @IntoMap
    @ViewModelKey(DeveloperDetailsViewModel::class)
    internal abstract fun bindDeveloperDetailsViewModel(userViewModel: DeveloperDetailsViewModel): ViewModel
}