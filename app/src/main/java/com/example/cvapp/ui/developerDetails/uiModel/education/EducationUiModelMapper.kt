package com.example.cvapp.ui.developerDetails.uiModel.education

import com.example.api.model.domain.education.Education
import javax.inject.Inject

class EducationUiModelMapper @Inject constructor(
    private val degreeUiModelMapper: DegreeUiModelMapper
) {

    fun map(education: List<Education>?): List<EducationUiModel> = education?.map(::map) ?: emptyList()

    fun map(education: Education) = with(education){
        EducationUiModel(
            schoolName = schoolName,
            degreesUiModel = degreeUiModelMapper.map(degrees),
            logoUrl = logoUrl,
            url = url
        )
    }
}