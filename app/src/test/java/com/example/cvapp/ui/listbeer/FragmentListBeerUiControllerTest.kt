package com.example.cvapp.ui.listbeer

import com.example.cvapp.ui.listbeer.uiComponent.FragmentListBeerUiComponent
import com.example.cvapp.ui.listbeer.uiModel.BeerItemUiModel
import io.mockk.*
import org.junit.Test

class FragmentListBeerUiControllerTest {

    private val sut = FragmentListBeerUiController()

    private val uiComponent: FragmentListBeerUiComponent = mockk()

    init {
        every { uiComponent.showLoading() } just runs
        sut.init(uiComponent)
    }

    @Test
    fun init() {
        verifySequence { uiComponent.showLoading() }
    }

    @Test
    fun onBoundBeerItems() {
        val beerItemUiModel: BeerItemUiModel = mockk()
        val listOfBeerItemUiModel = listOf(beerItemUiModel, beerItemUiModel)
        every { uiComponent.hideLoading() } just runs
        every { uiComponent.setNewListData(listOfBeerItemUiModel) } just runs

        sut.onBoundBeerItems(listOfBeerItemUiModel)

        verifySequence {
            uiComponent.showLoading()
            uiComponent.hideLoading()
            uiComponent.setNewListData(listOfBeerItemUiModel)
        }
    }

    @Test
    fun onBoundBeerItemClick() {
        val beerItemUiModel: BeerItemUiModel = mockk()
        every { beerItemUiModel.id } returns 1
        every { uiComponent.host.navigateToBeerDetails(any()) } just runs
        val a = FragmentListBeerDirections.actionBeerListToDetails(beerItemUiModel.id)
        sut.onBoundBeerItemClick(beerItemUiModel)
        verifySequence {
            uiComponent.showLoading()
            uiComponent.host.navigateToBeerDetails(any())
        }
    }
}